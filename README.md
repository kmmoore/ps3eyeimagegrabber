#Overview:#
Mac OS X 10.8 Mountain Lion compatible, delegate driven library to set parameters and grab frames from a PS3Eye. Uses a modified subset of the macam drivers.

Initial code modified from: https://github.com/jvcleave/PS3EyeWindow

#Status:#
 - Automatically detects cameras.
 - Can set 640x480 @ up to 60Hz or 320x240 @ up to 180Hz.
 - Delegate method will get called once per frame with an NSImageRep object.

#To Do:#
 - Clean existing code. Eventually this should be a clean PS3Eye-specific library, instead of the multi-camera library Macam is.
 - Fix the control/delegate structure. Including:
    - Add more delegate methods and an official protocol.
    - Provide a standard interface to control the camera.
 - Update the existing code to use the newer Quartz graphics instead of QuickDraw (which was removed in 10.8).
 - Find and update the rest of the deprecated code to hopefully keep it compiling into the future.