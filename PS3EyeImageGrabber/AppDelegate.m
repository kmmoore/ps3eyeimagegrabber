//
//  AppDelegate.m
//  PS3EyeImageGrabber
//
//  Created by Kyle Moore on 5/15/13.
//  Copyright (c) 2013 Kyle Moore. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

@synthesize cameraCentral, imageView;

- (void)dealloc
{
    [super dealloc];
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	self.cameraCentral = [MyCameraCentral sharedCameraCentral];
	self.cameraCentral.cameraResolution = ResolutionSIF;
	self.cameraCentral.cameraWidth = 320;
	self.cameraCentral.cameraHeight = 240;
	self.cameraCentral.cameraFPS = 180;
	
//	[central registerWrapper:this];
	[self.cameraCentral setDelegate:self];
	[self.cameraCentral startupWithNotificationsOnMainThread:YES recognizeLaterPlugins:YES];
}

- (void)cameraDetected:(unsigned long)cid {
	NSLog(@"[AppDelegate] Camera detected %lu", cid);
}

- (void)updateStatus:(NSString *)status fpsDisplay:(float)fpsDisplay fpsReceived:(float)fpsReceived {
	NSLog(@"%@, %g, %g", status, fpsDisplay, fpsReceived);
}

- (void)imageReady:(NSImageRep *)imageRep {
//	NSLog(@"%@", imageRep);
//	char *data = [imageRep bitmapData];
	NSImage * image = [[NSImage alloc] initWithSize:[imageRep size]];
	[image addRepresentation: imageRep];
	
	imageView.image = image;
	[image release];
	
}

@end
