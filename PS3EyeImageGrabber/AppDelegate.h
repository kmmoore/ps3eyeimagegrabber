//
//  AppDelegate.h
//  PS3EyeImageGrabber
//
//  Created by Kyle Moore on 5/15/13.
//  Copyright (c) 2013 Kyle Moore. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "MyCameraCentral.h"

@interface AppDelegate : NSObject <NSApplicationDelegate> {
	MyCameraCentral *cameraCentral;
	NSImageView *imageView;
}

@property (assign) IBOutlet NSWindow *window;
@property (assign) IBOutlet NSImageView *imageView;
@property (assign) MyCameraCentral *cameraCentral;

@end
