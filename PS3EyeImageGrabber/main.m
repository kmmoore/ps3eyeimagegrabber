//
//  main.m
//  PS3EyeImageGrabber
//
//  Created by Kyle Moore on 5/15/13.
//  Copyright (c) 2013 Kyle Moore. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
	return NSApplicationMain(argc, (const char **)argv);
}
